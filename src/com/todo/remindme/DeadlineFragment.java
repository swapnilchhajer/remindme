package com.todo.remindme;

import java.util.ArrayList;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class DeadlineFragment extends Fragment {

	ToDoListAdapter mAdapter;	
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		mAdapter = new ToDoListAdapter(getActivity());
		
		View rootView = inflater.inflate(R.layout.fragment_list, container, false);
		ListView listView = (ListView) rootView.findViewById(R.id.listView);
		
		loadItems();
			
        listView.setAdapter(mAdapter);
		return rootView;
	}
	
	// Load stored ToDoItems
	private void loadItems() {
		
		MySQLiteHelper db = new MySQLiteHelper(getActivity());
		ArrayList<ToDoItem> list = new ArrayList<ToDoItem>();
		list = db.getAllItems();
		
		for(ToDoItem l : list) {
			mAdapter.add(l);
		}
		db.close();
	}
	
	// Save ToDoItems to database
	public void appendData(ToDoItem newToDoItem) {
		mAdapter.add(newToDoItem);
		
		MySQLiteHelper db = new MySQLiteHelper(getActivity());
		db.addToDoItem(newToDoItem);
		db.close();	
	}	

}
