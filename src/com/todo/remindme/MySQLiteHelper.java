package com.todo.remindme;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import com.todo.remindme.ToDoItem.Priority;
import com.todo.remindme.ToDoItem.Status;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	// Database information
	private static final String DATABASE_NAME = "TasksDB";
	private static final int DATABASE_VERSION = 1;
	
	
	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		// Drop the old table if exists
		db.execSQL("DROP TABLE IF EXISTS tasks");
		
		// SQL statement to create book table
		String CREATE_TASK_TABLE = "CREATE TABLE tasks (" +
		"id INTEGER PRIMARY KEY AUTOINCREMENT, "+
		"title TEXT, "+
		"priority TEXT, "+
		"status TEXT,"+
		"date DATETIME)";
		
		//create books table
		db.execSQL(CREATE_TASK_TABLE);		
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older tables if existed
		db.execSQL("DROP TABLE IF EXISTS tasks");
		
		// Create fresh books table
		this.onCreate(db);
	}
	
    
	/**
     * CRUD operations (create "add", read "get", update, delete) book + get all books + delete all books
     */	

	// Books table name
	private static final String TABLE_TASKS = "tasks";
	
	// Books table column names
	private static final String KEY_ID = "id";
	private static final String KEY_TITLE = "title";
	private static final String KEY_PRIORITY = "priority";
	private static final String KEY_STATUS = "status";
	private static final String KEY_DATE = "date";
	
	private static final String[] COLUMNS = {KEY_ID, KEY_TITLE, KEY_PRIORITY, KEY_STATUS, KEY_DATE};	
	
	public void addToDoItem(ToDoItem item) {
		Log.d("addBook", item.toString());
		
		// 1. Get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();
		
		// 2. Create ContentValues to add key "column"/value
		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, item.getTitle());
		values.put(KEY_PRIORITY, item.getPriority().toString());
		values.put(KEY_STATUS, item.getStatus().toString());
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());		
		values.put(KEY_DATE, dateFormat.format(item.getDate()));
		
		// 3. Insert into the database
		db.insert(TABLE_TASKS, null, values);
		
		// 4. DB connection close
		db.close();
		
	}
	
	public ToDoItem getToDoItem(int id) {
		ToDoItem item = null;
		
		// 1. Get reference to readable DB
		SQLiteDatabase db = this.getReadableDatabase();
		
		// 2. Build query
		Cursor cursor = db.query(TABLE_TASKS, COLUMNS, " id = ? ", new String[] {String.valueOf(id)}, null, null, null, null);
		
		// 3. If we got the results get the first one
		if (cursor != null) {
			cursor.moveToFirst();
		}
		
		// 4. Build book object
		item = new ToDoItem();
		
		item.setTitle(cursor.getString(1));
		
		String priority = cursor.getString(2);
		if(priority == "HIGH") {
			item.setPriority(Priority.HIGH);			
		}
		else if(priority == "MED") {
			item.setPriority(Priority.MED);
		}
		else {
			item.setPriority(Priority.LOW);
		}

		if(cursor.getString(3) == "DONE") {
			item.setStatus(Status.DONE);
		}
		else {
			item.setStatus(Status.NOTDONE);
		}
		
		// log
		Log.d("getBook("+id+")", item.toString());
		
		// 5. Return book	
		return item;
	}
	
	public ArrayList<ToDoItem> getAllItems() {
		ArrayList<ToDoItem> items = new ArrayList<ToDoItem>();
		
		// 1. Build the query
		String query = "SELECT * FROM " + TABLE_TASKS;
		
		// 2. Get reference to readable DB
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(query, null);
		
		// 3. Go over each row, build row and add it to list
		ToDoItem item = null;
		if(cursor.moveToFirst()) {
			do {
				item = new ToDoItem();				
				item.setTitle(cursor.getString(1));
				
				String priority = cursor.getString(2);
				if(priority == "HIGH") {
					item.setPriority(Priority.HIGH);			
				}
				else if(priority == "MED") {
					item.setPriority(Priority.MED);
				}
				else {
					item.setPriority(Priority.LOW);
				}

				if(cursor.getString(3) == "DONE") {
					item.setStatus(Status.DONE);
				}
				else {
					item.setStatus(Status.NOTDONE);
				}
				
				// Add book to books
				items.add(item);
			} while(cursor.moveToNext());
		}
		
		Log.d("getAllItems", items.toString());
				
		// 4. Return books
		return items;
	}
	
	/* 
	 * Function: update
	 * Input: Book which we need to update
	 * Return: The number of rows affected
	 */
	public int update(ToDoItem item) {
		
		// 1. Get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();
		
		// 2. Create ContentValues to add key "column"/value 
		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, item.getTitle()); //get title
		values.put(KEY_PRIORITY, item.getPriority().toString()); //get priority
		values.put(KEY_STATUS, item.getStatus().toString()); //get status
		
		// 3. Updating row
		int numRowsAffected = db.update(TABLE_TASKS, values, KEY_ID+" = ?", new String[]{String.valueOf(item.getId())});
		
		// 4. close
		db.close();
		
		return numRowsAffected;
		
	}
	
	public void deleteBook(ToDoItem item) {
		
		// 1. Get reference to writable DB
		SQLiteDatabase db = this.getWritableDatabase();
		
		// 2. Delete query
		db.delete(TABLE_TASKS, KEY_ID+" = ?", new String[] {String.valueOf(item.getId())});
		
		// 3. close
		db.close();
		
		Log.d("deleteToDoItem", item.toString());
		
	}
	
	public void deleteAllItems() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.rawQuery("DELETE * FROM TasksDB", null);
	}

}
